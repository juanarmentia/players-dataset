import pandas as pd
import requests
from bs4 import BeautifulSoup
from configuration import Configuration
import copy

conf = Configuration()

leagueIndex = 1

for league in conf.leagues_transfermarkt:
    tree = requests.get(league, headers=conf.headers_transfermarkt)
    soup = BeautifulSoup(tree.content, 'html.parser')
    leagueName = soup.find_all("h1")[0].text
    print(leagueName)

    # Create an empty list to assign these values to
    teamLinks = []

    # Extract all links with the correct CSS selector
    links = soup.select("a.vereinprofil_tooltip")

    # We need the location that the link is pointing to, so for each link, take the link location.
    # Additionally, we only need the links in locations 1,3,5,etc. of our list, so loop through those only
    for link in links:
        teamLinks.append(link.get("href"))

    # Remove duplicates
    teamLinks = list(dict.fromkeys(teamLinks))
    aux_team_links = copy.copy(teamLinks)

    for team_link in aux_team_links:
        if 'startseite' not in team_link:
            teamLinks.remove(team_link)

    # For each location that we have taken, add the website before it - this allows us to call it later
    for i in range(len(teamLinks)):
        teamLinks[i] = "https://www.transfermarkt.es" + teamLinks[i]

    # teamLinks[14]

    # Create an empty list for our player links to go into
    playerLinks = []

    # Run the scraper through each of our 20 team links
    for i in range(len(teamLinks)):

        # Download and process the team page
        page = teamLinks[i]
        tree = requests.get(page, headers=conf.headers_transfermarkt)
        soup = BeautifulSoup(tree.content, 'html.parser')

        # Extract all links
        links = soup.select("a.spielprofil_tooltip")

        # For each link, extract the location that it is pointing to
        for j in range(len(links)):
            playerLinks.append(links[j].get("href"))

        # Add the location to the end of the transfermarkt domain to make it ready to scrape
        for j in range((len(playerLinks)-len(links)),len(playerLinks)):
            playerLinks[j] = "https://www.transfermarkt.es" + playerLinks[j]

        # The page list the players more than once - let's use list(set(XXX)) to remove the duplicates
        playerLinks = list(set(playerLinks))

    print(len(playerLinks))

    players = []

    for i in range(len(playerLinks)):
        try:
            # Take site and structure html
            page = playerLinks[i]
            tree = requests.get(page, headers=conf.headers_transfermarkt)
            soup = BeautifulSoup(tree.content, 'html.parser')

            # Find image and save it with the player's name
            # Find the player's name
            playerName = soup.find_all("h1")[0].text
            print(playerName)

            playerData = soup.select(".auflistung td")
            titles = soup.select(".auflistung th")
            index = 0
            birthDateIndex = None
            ageIndex = None
            heightIndex = None
            nationalityIndex = None
            positionIndex = None
            feetIndex = None
            clubIndex = None
            contractIndex = None

            for title in titles:
                title_text = title.text.strip('\t\n\r').strip(' ').strip('\n')
                if title_text == 'Fecha de nacimiento:':
                    birthDateIndex = index
                elif title_text == 'Edad:':
                    ageIndex = index
                elif title_text == 'Altura:':
                    heightIndex = index
                elif title_text == 'Nacionalidad:':
                    nationalityIndex = index
                elif title_text == 'Posición:':
                    positionIndex = index
                elif title_text == 'Pie:':
                    feetIndex = index
                elif title_text == 'Club actual:':
                    clubIndex = index
                elif title_text == 'Contrato hasta:':
                    contractIndex = index

                index += 1

            playerValueObj = soup.select(".zeile-oben .right-td")
            if len(playerValueObj) == 0:
                playerValueObj = soup.find_all('div', class_='dataMarktwert')[0].select('a')[0].text
                playerValueStr = playerValueObj.strip('\r\t\n').strip(' ')
            else:
                playerValueStr = playerValueObj[0].text.strip('\r\t\n').strip(' ')

            playerValueStr = playerValueStr.replace(",", ".")
            if playerValueStr.find('mill') != -1:
                playerValue = int(float(playerValueStr.split(' ')[0]) * 1000000)
            elif playerValueStr.find('miles') != -1:
                playerValue = int(float(playerValueStr.split(' ')[0]) * 1000)
            else:
                playerValue = 0

            playerMaxValueObj = soup.select(".zeile-unten .right-td")
            if len(playerMaxValueObj) == 0:
                playerMaxValue = playerValue
            else:
                playerMaxValueStr = playerMaxValueObj[0].text.strip('\r\t\n').split('\n')[0].strip('\t\n\r').strip(' ')
                playerMaxValueStr = playerMaxValueStr.replace(",", ".")
                if playerMaxValueStr.find('mill') != -1:
                    playerMaxValue = int(float(playerMaxValueStr.split(' ')[0]) * 1000000)
                elif playerMaxValueStr.find('miles') != -1:
                    playerMaxValue = int(float(playerMaxValueStr.split(' ')[0]) * 1000)
                else:
                    playerMaxValue = 0

            playerBirthDate = playerData[birthDateIndex].text if birthDateIndex is not None else ''
            playerAge = playerData[ageIndex].text if ageIndex is not None else ''
            playerHeigth = playerData[heightIndex].text.replace(',', '')[0:3] if heightIndex is not None else ''

            nationalityIterator = 1
            if nationalityIndex is not None:
                nationalities = playerData[nationalityIndex].find_all('img')
                nationality = nationalities[0]['title']
                while nationalityIterator < len(nationalities):
                    nationality = nationality + ';' + nationalities[nationalityIterator]['title']
                    nationalityIterator += 1
                playerNationality = nationality
            else:
                playerNationality = ''

            playerPosition = playerData[positionIndex].text.strip('\r\t\n').strip(' ').split(' - ') if positionIndex is not None else None
            playerPosition1 = playerPosition[0] if playerPosition is not None and len(playerPosition) > 0 else ''
            playerPosition2 = playerPosition[1] if playerPosition is not None and len(playerPosition) > 1 else ''
            playerPosition3 = playerPosition[2] if playerPosition is not None and len(playerPosition) > 2 else ''
            playerPosition4 = playerPosition[3] if playerPosition is not None and len(playerPosition) > 3 else ''

            playerFeet = playerData[feetIndex].text if feetIndex is not None else ''
            playerClub = playerData[clubIndex].text.strip('\n ') if clubIndex is not None else ''
            playerContract = playerData[contractIndex].text if contractIndex is not None else ''

            player_tuple = {'Nombre':playerName, 'Fecha de nacimiento': playerBirthDate, 'Edad':playerAge,
                            'Altura': playerHeigth, 'Nacionalidad': playerNationality, 'Posicion1': playerPosition1,
                            'Posicion2': playerPosition2, 'Posicion3': playerPosition3, 'Posicion4': playerPosition4,
                            'Pie': playerFeet, 'Club': playerClub, 'Fin contrato': playerContract, 'Valor':playerValue,
                            'ValorMax':playerMaxValue}

            players.append(player_tuple)
            print(len(players))
        except Exception as e:
            print(e)
            print(playerData)
            print(titles)
            print(playerValueObj)

    df = pd.DataFrame.from_dict(players)

    print(df)

    fileName = '../output/transfermarkt_' + leagueName + '.csv'
    leagueIndex += 1
    df.to_csv(fileName, index=False)

