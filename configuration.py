

class Configuration:
    headers_transfermarkt = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/47.0.2526.106 Safari/537.36'}
    leagues_transfermarkt = ['https://www.transfermarkt.es/primera-division/startseite/wettbewerb/ES1',  # LaLiga
               # 'https://www.transfermarkt.es/segunda-division/startseite/wettbewerb/ES2',  # LaLiga 2
               # 'https://www.transfermarkt.es/segunda-division-b-grupo-i/startseite/wettbewerb/ES3A',  # 2B-1
               # 'https://www.transfermarkt.es/segunda-division-b-grupo-ii/startseite/wettbewerb/ES3B',  # 2B-2
               # 'https://www.transfermarkt.es/segunda-division-b-grupo-iii/startseite/wettbewerb/ES3C',  # 2B-3
               # 'https://www.transfermarkt.es/segunda-division-b-grupo-iv/startseite/wettbewerb/ES3D',  # 2B-4
               # 'https://www.transfermarkt.es/1-bundesliga/startseite/wettbewerb/L1',  # Bundesliga 1
               # 'https://www.transfermarkt.es/2-bundesliga/startseite/wettbewerb/L2',  # Bundesliga 2
               # 'https://www.transfermarkt.es/3-bundesliga/startseite/wettbewerb/L3',  # Bundesliga 3
               # 'https://www.transfermarkt.es/premier-league/startseite/wettbewerb/GB1',  # Premier League
               # 'https://www.transfermarkt.es/championship/startseite/wettbewerb/GB2',  # Championship
               # 'https://www.transfermarkt.es/league-one/startseite/wettbewerb/GB3',  # League One
               # 'https://www.transfermarkt.es/serie-a/startseite/wettbewerb/IT1',  # Serie A
               # 'https://www.transfermarkt.es/serie-a/startseite/wettbewerb/IT2',  # Serie B
               # 'https://www.transfermarkt.es/ligue-1/startseite/wettbewerb/FR1',  # Ligue 1
               # 'https://www.transfermarkt.es/ligue-1/startseite/wettbewerb/FR2',  # Ligue 2
               # 'https://www.transfermarkt.es/liga-nos/startseite/wettbewerb/PO1',  # Portugal 1
               # 'https://www.transfermarkt.es/segunda-liga/startseite/wettbewerb/PO2',  # Portugal 2
               # 'https://www.transfermarkt.es/eredivisie/startseite/wettbewerb/NL1',  # Holanda 1
               # 'https://www.transfermarkt.es/jupiler-league/startseite/wettbewerb/NL2',  # Holanda 2
               # 'https://www.transfermarkt.es/jupiler-pro-league/startseite/wettbewerb/BE1',  # Bélgica
               # 'https://www.transfermarkt.es/super-league/startseite/wettbewerb/C1',  # Suiza
               # 'https://www.transfermarkt.es/premier-liga/startseite/wettbewerb/RU1',  # Rusia 1
               # 'https://www.transfermarkt.es/1-division/startseite/wettbewerb/RU2',  # Rusia 2
               # 'https://www.transfermarkt.es/tippeligaen/startseite/wettbewerb/NO1',  # Noruega
               # 'https://www.transfermarkt.es/allsvenskan/startseite/wettbewerb/SE1',  # Suecia
               # 'https://www.transfermarkt.es/veikkausliiga/startseite/wettbewerb/FI1',  # Finlandia
               # 'https://www.transfermarkt.es/scottish-premiership/startseite/wettbewerb/SC1',  # Escocia
               # 'https://www.transfermarkt.es/super-lig/startseite/wettbewerb/TR1',  # Turquía
               # 'https://www.transfermarkt.es/premier-liga/startseite/wettbewerb/UKR1',  # Ucrania
               # 'https://www.transfermarkt.es/wyschejschaja-liha/startseite/wettbewerb/WER1',  # Bielorrusia
               # 'https://www.transfermarkt.es/premium-liiga/startseite/wettbewerb/EST1',  # Estonia
               # 'https://www.transfermarkt.es/virsliga/startseite/wettbewerb/LET1',  # Letonia
               # 'https://www.transfermarkt.es/a-lyga/startseite/wettbewerb/LI1',  # Lituania
               # 'https://www.transfermarkt.es/divizia-nationala-ab-2017-/startseite/wettbewerb/MO1N',  # Moldavia
               # 'https://www.transfermarkt.es/liga-1/startseite/wettbewerb/RO1',  # Rumania
               # 'https://www.transfermarkt.es/a-grupa/startseite/wettbewerb/BU1',  # Bulgaria
               # 'https://www.transfermarkt.es/super-league/startseite/wettbewerb/GR1',  # Grecia
               # 'https://www.transfermarkt.es/prva-makedonska-fudbalska-liga/startseite/wettbewerb/MAZ1',  # Macedonia
               # 'https://www.transfermarkt.es/kategoria-superiore/startseite/wettbewerb/ALB1',  # Albania
               # 'https://www.transfermarkt.es/vala-superliga/startseite/wettbewerb/KO1',  # Kosovo
               # 'https://www.transfermarkt.es/prva-crnogorska-liga/startseite/wettbewerb/MNE1',  # Montenegro
               # 'https://www.transfermarkt.es/superliga/startseite/wettbewerb/SER1',  # Serbia
               # 'https://www.transfermarkt.es/premijer-liga/startseite/wettbewerb/BOS1',  # Bosnia
               # 'https://www.transfermarkt.es/1-hnl/startseite/wettbewerb/KR1',  # Croacia
               # 'https://www.transfermarkt.es/nemzeti-bajnoksag/startseite/wettbewerb/UNG1',  # Hungría
               # 'https://www.transfermarkt.es/corgon-liga/startseite/wettbewerb/SLO1',  # Eslovaquia
               # 'https://www.transfermarkt.es/ekstraklasa/startseite/wettbewerb/PL1',  # Polonia
               # 'https://www.transfermarkt.es/gambrinus-liga/startseite/wettbewerb/TS1',  # República Checa
               # 'https://www.transfermarkt.es/bundesliga/startseite/wettbewerb/A1',  # Austria
               # 'https://www.transfermarkt.es/prva-liga/startseite/wettbewerb/SL1',  # Eslovenia
               # 'https://www.transfermarkt.es/superligaen/startseite/wettbewerb/DK1',  # Dinamarca
               # 'https://www.transfermarkt.es/bgl-ligue/startseite/wettbewerb/LUX1',  # Luxemburgo
               # 'https://www.transfermarkt.es/liga-mx-apertura/startseite/wettbewerb/MEXA',  # México
               # 'https://www.transfermarkt.es/primera-division-apertura/startseite/wettbewerb/CRPD',  # Costa Rica
               # 'https://www.transfermarkt.es/liga-venezolana-apertura/startseite/wettbewerb/VZ1A',  # Venezuela
               # 'https://www.transfermarkt.es/liga-postobon-i/startseite/wettbewerb/COLP',  # Colombia
               # 'https://www.transfermarkt.es/primera-divison/startseite/wettbewerb/CLPD',  # Chile
               # 'https://www.transfermarkt.es/primera-division-apertura/startseite/wettbewerb/URU1',  # Uruguay
               # 'https://www.transfermarkt.es/primera-division-apertura/startseite/wettbewerb/PR1A',  # Paraguay
               # 'https://www.transfermarkt.es/ligapro-serie-a-primera-etapa/startseite/wettbewerb/EL1A',  # Ecuador
               # 'https://www.transfermarkt.es/torneo-descentralizado-apertura/startseite/wettbewerb/TDeA',  # Peru
               # 'https://www.transfermarkt.es/major-league-soccer/startseite/wettbewerb/MLS1',  # USA
               # 'https://www.transfermarkt.es/canadian-premier-league/startseite/wettbewerb/CDN1',  # Canada
               # 'https://www.transfermarkt.es/primera-division/startseite/wettbewerb/AR1N',  # Argentina
               # 'https://www.transfermarkt.es/campeonato-brasileiro-serie-a/startseite/wettbewerb/BRA1'  # Brasil
            ]

    leagues_fbref = ['https://fbref.com/es/comps/12/stats/La-Liga-Stats',  # LaLiga
               # 'https://fbref.com/es/comps/17/stats/La-Liga-2-Stats',  # LaLiga 2
               # '',  # 2B-1
               # '',  # 2B-2
               # '',  # 2B-3
               # '',  # 2B-4
               # 'https://fbref.com/es/comps/20/stats/Bundesliga-Stats',  # Bundesliga 1
               # 'https://fbref.com/es/comps/33/stats/2-Bundesliga-Stats',  # Bundesliga 2
               # 'https://fbref.com/es/comps/59/stats/3-Liga-Stats',  # Bundesliga 3
               # 'https://fbref.com/es/comps/9/stats/Premier-League-Stats',  # Premier League
               # 'https://fbref.com/es/comps/10/stats/Championship-Stats',  # Championship
               # 'https://fbref.com/es/comps/15/stats/League-One-Stats',  # League One
               # 'https://fbref.com/es/comps/11/stats/Serie-A-Stats',  # Serie A
               # 'https://fbref.com/es/comps/18/stats/Serie-B-Stats',  # Serie B
               # 'https://fbref.com/es/comps/13/stats/Ligue-1-Stats',  # Ligue 1
               # 'https://fbref.com/es/comps/60/stats/Ligue-2-Stats',  # Ligue 2
               # 'https://fbref.com/es/comps/32/stats/Primeira-Liga-Stats',  # Portugal 1
               # '',  # Portugal 2
               # 'https://fbref.com/es/comps/23/stats/Dutch-Eredivisie-Stats',  # Holanda 1
               # 'https://fbref.com/es/comps/51/stats/Dutch-Eerste-Divisie-Stats',  # Holanda 2
               # 'https://fbref.com/es/comps/37/stats/Belgian-First-Division-A-Stats',  # Bélgica
               # 'https://fbref.com/es/comps/57/stats/Super-League-Stats',  # Suiza
               # 'https://fbref.com/es/comps/30/stats/Russian-Premier-League-Stats',  # Rusia 1
               # '',  # Rusia 2
               # 'https://fbref.com/es/comps/28/stats/Eliteserien-Stats',  # Noruega
               # 'https://fbref.com/es/comps/29/stats/Allsvenskan-Stats',  # Suecia
               # 'https://fbref.com/es/comps/43/stats/Veikkausliiga-Stats',  # Finlandia
               # 'https://fbref.com/es/comps/40/stats/Scottish-Premiership-Stats',  # Escocia
               # 'https://fbref.com/es/comps/26/stats/Super-Lig-Stats',  # Turquía
               # 'https://fbref.com/es/comps/39/stats/Ukrainian-Premier-League-Stats',  # Ucrania
               # '',  # Bielorrusia
               # '',  # Estonia
               # '',  # Letonia
               # '',  # Lituania
               # '',  # Moldavia
               # 'https://fbref.com/es/comps/47/stats/Liga-I-Stats',  # Rumania
               # 'https://fbref.com/es/comps/67/stats/Bulgarian-First-League-Stats',  # Bulgaria
               # 'https://fbref.com/es/comps/27/stats/Super-League-Greece-Stats',  # Grecia
               # '',  # Macedonia
               # '',  # Albania
               # '',  # Kosovo
               # '',  # Montenegro
               # 'https://fbref.com/es/comps/54/stats/Serbian-SuperLiga-Stats',  # Serbia
               # '',  # Bosnia
               # 'https://fbref.com/es/comps/63/stats/1-HNL-Stats',  # Croacia
               # 'https://fbref.com/es/comps/46/stats/NB-I-Stats',  # Hungría
               # '',  # Eslovaquia
               # 'https://fbref.com/es/comps/36/stats/Ekstraklasa-Stats',  # Polonia
               # 'https://fbref.com/es/comps/66/stats/Czech-First-League-Stats',  # República Checa
               # 'https://fbref.com/es/comps/56/stats/Austrian-Bundesliga-Stats',  # Austria
               # '',  # Eslovenia
               # 'https://fbref.com/es/comps/50/stats/Superliga-Stats',  # Dinamarca
               # '',  # Luxemburgo
               # 'https://fbref.com/es/comps/31/stats/Liga-MX-Stats',  # México
               # '',  # Costa Rica
               # 'https://fbref.com/es/comps/105/4012/stats/2019-Primera-Division-Stats',  # Venezuela
               # 'https://fbref.com/es/comps/41/3927/stats/2019-Primera-A-Stats',  # Colombia
               # 'https://fbref.com/es/comps/35/3926/stats/2019-Primera-Division-Stats',  # Chile
               # 'https://fbref.com/es/comps/45/3929/stats/2019-Primera-Division-Stats',  # Uruguay
               # 'https://fbref.com/es/comps/61/3932/stats/2019-Primera-Division-Stats',  # Paraguay
               # 'https://fbref.com/es/comps/58/3931/stats/2019-Serie-A-Stats',  # Ecuador
               # 'https://fbref.com/es/comps/44/3928/stats/2019-Liga-1-Stats',  # Peru
               # 'https://fbref.com/es/comps/22/2798/stats/2019-Major-League-Soccer-Stats',  # USA
               # '',  # Canada
               # 'https://fbref.com/es/comps/21/stats/Superliga-Argentina-Stats',  # Argentina
               # 'https://fbref.com/es/comps/24/stats/Serie-A-Stats'  # Brasil
               ]

    leagues_understat = ['https://understat.com/league/La_liga/2019',  # LaLiga
                     '',  # LaLiga 2
                     '',  # 2B-1
                     '',  # 2B-2
                     '',  # 2B-3
                     '',  # 2B-4
                     'https://understat.com/league/Bundesliga/2019',  # Bundesliga 1
                     '',  # Bundesliga 2
                     '',  # Bundesliga 3
                     'https://understat.com/league/EPL/2019',  # Premier League
                     '',  # Championship
                     '',  # League One
                     'https://understat.com/league/Serie_A/2019',  # Serie A
                     '',  # Serie B
                     'https://understat.com/league/Ligue_1/2019',  # Ligue 1
                     '',  # Ligue 2
                     '',  # Portugal 1
                     '',  # Portugal 2
                     '',  # Holanda 1
                     '',  # Holanda 2
                     '',  # Bélgica
                     '',  # Suiza
                     'https://understat.com/league/RFPL/2019',  # Rusia 1
                     '',  # Rusia 2
                     '',  # Noruega
                     '',  # Suecia
                     '',  # Finlandia
                     '',  # Escocia
                     '',  # Turquía
                     '',  # Ucrania
                     '',  # Bielorrusia
                     '',  # Estonia
                     '',  # Letonia
                     '',  # Lituania
                     '',  # Moldavia
                     '',  # Rumania
                     '',  # Bulgaria
                     '',  # Grecia
                     '',  # Macedonia
                     '',  # Albania
                     '',  # Kosovo
                     '',  # Montenegro
                     '',  # Serbia
                     '',  # Bosnia
                     '',  # Croacia
                     '',  # Hungría
                     '',  # Eslovaquia
                     '',  # Polonia
                     '',  # República Checa
                     '',  # Austria
                     '',  # Eslovenia
                     '',  # Dinamarca
                     '',  # Luxemburgo
                     '',  # México
                     '',  # Costa Rica
                     '',  # Venezuela
                     '',  # Colombia
                     '',  # Chile
                     '',  # Uruguay
                     '',  # Paraguay
                     '',  # Ecuador
                     '',  # Peru
                     '',  # USA
                     '',  # Canada
                     '',  # Argentina
                     '']  # Brasil

    headers_sofascore = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/47.0.2526.106 Safari/537.36'}
    leagues_sofascore = ['https://www.sofascore.com/es/torneo/futbol/spain/laliga/8',  # LaLiga
                         # 'https://www.sofascore.com/es/torneo/futbol/spain/laliga-2/54',  # LaLiga 2
                         # '',  # 2B-1
                         # '',  # 2B-2
                         # '',  # 2B-3
                         # '',  # 2B-4
                         # 'https://www.sofascore.com/es/torneo/futbol/germany/bundesliga/35',  # Bundesliga 1
                         # 'https://www.sofascore.com/es/torneo/futbol/germany/2-bundesliga/44',  # Bundesliga 2
                         # '',  # Bundesliga 3
                         # 'https://www.sofascore.com/es/torneo/futbol/england/premier-league/17',  # Premier League
                         # 'https://www.sofascore.com/es/torneo/futbol/england/championship/18',  # Championship
                         # 'https://www.sofascore.com/es/torneo/futbol/england/league-one/24',  # League One
                         # 'https://www.sofascore.com/es/torneo/futbol/italy/serie-a/23',  # Serie A
                         # 'https://www.sofascore.com/es/torneo/futbol/italy/serie-b/53',  # Serie B
                         # 'https://www.sofascore.com/es/torneo/futbol/france/ligue-1/34',  # Ligue 1
                         # 'https://www.sofascore.com/es/torneo/futbol/france/ligue-2/182',  # Ligue 2
                         # 'https://www.sofascore.com/es/torneo/futbol/portugal/primeira-liga/238',  # Portugal 1
                         # '',  # Portugal 2
                         # 'https://www.sofascore.com/es/torneo/futbol/netherlands/eredivisie/37',  # Holanda 1
                         # '',  # Holanda 2
                         # 'https://www.sofascore.com/es/torneo/futbol/belgium/first-division-a/38',  # Bélgica
                         # 'https://www.sofascore.com/es/torneo/futbol/switzerland/super-league/215',  # Suiza
                         # 'https://www.sofascore.com/es/torneo/futbol/russia/premier-liga/203',  # Rusia 1
                         # '',  # Rusia 2
                         # 'https://www.sofascore.com/es/torneo/futbol/norway/eliteserien/20',  # Noruega
                         # 'https://www.sofascore.com/es/torneo/futbol/sweden/allsvenskan/40',  # Suecia
                         # '',  # Finlandia
                         # '',  # Escocia
                         # 'https://www.sofascore.com/es/torneo/futbol/turkey/super-lig/52',  # Turquía
                         # '',  # Ucrania
                         # 'https://www.sofascore.com/es/torneo/futbol/belarus/vysshaya-liga/169',  # Bielorrusia
                         # '',  # Estonia
                         # '',  # Letonia
                         # '',  # Lituania
                         # '',  # Moldavia
                         # '',  # Rumania
                         # 'https://www.sofascore.com/es/torneo/futbol/bulgaria/parva-liga/247',  # Bulgaria
                         # '',  # Grecia
                         # '',  # Macedonia
                         # '',  # Albania
                         # '',  # Kosovo
                         # '',  # Montenegro
                         # '',  # Serbia
                         # '',  # Bosnia
                         # '',  # Croacia
                         # '',  # Hungría
                         # '',  # Eslovaquia
                         # '',  # Polonia
                         # '',  # República Checa
                         # 'https://www.sofascore.com/es/torneo/futbol/austria/bundesliga/45',  # Austria
                         # '',  # Eslovenia
                         # 'https://www.sofascore.com/es/torneo/futbol/denmark/superliga/39',  # Dinamarca
                         # '',  # Luxemburgo
                         # 'https://www.sofascore.com/es/torneo/futbol/mexico/liga-mx-apertura/11621',  # México
                         # '',  # Costa Rica
                         # '',  # Venezuela
                         # 'https://www.sofascore.com/es/torneo/futbol/colombia/primera-a-apertura/11539',  # Colombia
                         # 'https://www.sofascore.com/es/torneo/futbol/chile/primera-division/11653',  # Chile
                         # '',  # Uruguay
                         # '',  # Paraguay
                         # '',  # Ecuador
                         # 'https://www.sofascore.com/es/torneo/futbol/peru/liga-1/406',  # Peru
                         # 'https://www.sofascore.com/es/torneo/futbol/usa/mls/242',  # USA
                         # '',  # Canada
                         # 'https://www.sofascore.com/es/torneo/futbol/argentina/superliga/155',  # Argentina
                         # 'https://www.sofascore.com/es/torneo/futbol/brazil/brasileiro-serie-a/325'  # Brasil
                         ]


                         # "https://www.sofascore.com/es/torneo/futbol/bolivia/division-profesional-apertura/11684"  #Bolivia
