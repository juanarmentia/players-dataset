import pandas as pd
from fuzzywuzzy import process

transfermarkt_df = pd.read_csv('output/transfermarkt_LaLiga.csv')
sofascore_df = pd.read_csv('output/sofascore_LaLiga.csv')
fbref_df = pd.read_csv('output/FBref_La-Liga.csv')

transfermarkt_df = transfermarkt_df.rename(columns={"Nombre": "player"})
sofascore_df = sofascore_df.rename(columns={"Nombre": "player"})

sofascore_df['player'] = sofascore_df.player.apply(
    lambda x: [process.extract(x, transfermarkt_df.player, limit=1)][0][0][0])

df_left_sofascore_transfermarkt = pd.merge(sofascore_df, transfermarkt_df, on='player', how='left')

sofascore_transfermarkt_csv = df_left_sofascore_transfermarkt.to_csv('output/sofascore_transfermarkt_csv.csv', index=False)

# fbref_df['player'] = fbref_df.player.apply(
#     lambda x: [process.extract(x, transfermarkt_df.player, limit=1)][0][0][0])
#
#
# df_left_fbref_transfermarkt = pd.merge(fbref_df, transfermarkt_df, on='player', how='left')
#
# # intermediate_csv = df_left_fbref_transfermarkt.to_csv('output/inter.csv', index=False)
#
# df_left_fbref_transfermarkt['player'] = df_left_fbref_transfermarkt.player.apply(
#     lambda x: [process.extract(x, sofascore_df.player, limit=1)][0][0][0])
#
# df_left_fbref_transfermarkt_sofascore = pd.merge(df_left_fbref_transfermarkt, sofascore_df, on='player', how='left')
#
# join_csv = df_left_fbref_transfermarkt_sofascore.to_csv('output/join.csv', index=False)
