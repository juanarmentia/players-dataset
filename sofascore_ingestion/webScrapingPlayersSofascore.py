import pandas as pd
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from configuration import Configuration
import time

conf = Configuration()

leagueIndex = 1

for league in conf.leagues_sofascore:
    url = league
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    time.sleep(3)
    page = driver.page_source
    driver.quit()
    soup = BeautifulSoup(page, 'html.parser')

    leagueName = soup.find_all("h2")[0].text
    print(leagueName)

    # Create an empty list to assign these values to
    teamLinks = []

    ahref_list = soup.findAll('a')

    for link in ahref_list:
        if link.get('href') is not None and 'equipo' in link.get('href'):
            teamLinks.append(link.get("href"))

    # Remove duplicates
    teamLinks = list(dict.fromkeys(teamLinks))

    # For each location that we have taken, add the website before it - this allows us to call it later
    for i in range(len(teamLinks)):
        teamLinks[i] = "https://www.sofascore.com" + teamLinks[i]

    # teamLinks[14]

    # Create an empty list for our player links to go into
    playerLinks = []


    # Run the scraper through each of our 20 team links
    # for i in range(len(teamLinks)):
    for i in range(1):
        playerPartLinks = []
        # Download and process the team page
        url = teamLinks[i]
        driver_team = webdriver.Chrome(options=options)
        driver_team.get(url)
        time.sleep(3)
        page_team = driver_team.page_source
        driver_team.quit()
        soup_team = BeautifulSoup(page_team, 'html.parser')

        # Extract all links
        ahref_links = soup_team.select("a")

        # For each link, extract the location that it is pointing to
        for link in ahref_links:
            if link.get('href') is not None and 'jugador' in link.get('href'):
                playerPartLinks.append(link.get("href"))

        # Add the location to the end of the transfermarkt domain to make it ready to scrape
        # for j in range((len(playerLinks)-len(ahref_links)),len(playerLinks)):
        #     playerLinks[j] = "https://www.transfermarkt.es" + playerLinks[j]

        # Remove duplicates
        playerPartLinks = list(dict.fromkeys(playerPartLinks))

        # For each location that we have taken, add the website before it - this allows us to call it later
        for j in range(len(playerPartLinks)):
            playerLinks.append("https://www.sofascore.com" + playerPartLinks[j])

        print(len(playerLinks))

    print(len(playerLinks))
    print(playerLinks)

    players = []

    for i in range(len(playerLinks)):
        try:
            # Take site and structure html
            url = playerLinks[i]
            driver_player = webdriver.Chrome(options=options)
            driver_player.get(url)
            time.sleep(3)
            page_player = driver_player.page_source
            driver_player.quit()
            soup_player = BeautifulSoup(page_player, 'html.parser')

            url_splitted = url.split('/')
            player_id = url_splitted[len(url_splitted)-1]
            player_img = 'https://www.sofascore.com/images/player/image_' + player_id + '.png'

            # Find the player's name
            playerName = ''
            span_list = soup_player.select('span')
            for span in span_list:
                if span.get('class') is not None and 'PlayerName' in span.get('class')[0]:
                    playerName = span.text
                    break

            print(playerName)

            tables = soup_player.select('table')

            eventos = tables[0].tbody.select('tr')
            porteria = None
            if len(tables) == 6:
                atacante = tables[1].tbody.select('tr')
                pases = tables[2].tbody.select('tr')
                defensa = tables[3].tbody.select('tr')
                otros = tables[4].tbody.select('tr')
                tarjetas = tables[5].tbody.select('tr')
            elif len(tables) == 7:
                porteria = tables[1].tbody.select('tr')
                atacante = tables[2].tbody.select('tr')
                pases = tables[3].tbody.select('tr')
                defensa = tables[4].tbody.select('tr')
                otros = tables[5].tbody.select('tr')
                tarjetas = tables[6].tbody.select('tr')
            else:
                print("Jugador a ver detalle: ", playerName)


            eventos_tuple = {}
            for evento in eventos:
                eventos_tuple['Evento ' + evento.select('td')[0].text] = evento.select('td')[1].text

            if porteria is not None:
                porteria_tuple = {}
                for port in porteria:
                    if port.select('td')[0].text == 'Salvadas por juego' or \
                            port.select('td')[0].text == 'Salidas con éxito por partido':
                        porteria_tuple['Portería ' + port.select('td')[0].text] = port.select('td')[1].text.split(' ')[0]
                        porteria_tuple['Portería Porc.' + port.select('td')[0].text] = int(port.select('td')[1].text.
                                                                                  split(' ')[1].strip('(').strip(')').
                                                                                  strip('%')) / 100
                    else:
                        porteria_tuple['Portería ' + port.select('td')[0].text] = port.select('td')[1].text

            atacante_tuple = {}
            for ataque in atacante:
                if ataque.select('td')[0].text == 'Frecuencia de goles':
                    atacante_tuple['Ataque ' + ataque.select('td')[0].text + ' min'] = ataque.select('td')[1].text.split(' ')[0]
                else:
                    atacante_tuple['Ataque ' + ataque.select('td')[0].text] = ataque.select('td')[1].text

            pases_tuple = {}
            for pase in pases:
                if pase.select('td')[0].text == 'Completados por partido' or \
                        pase.select('td')[0].text == 'Comp. campo propio' or \
                        pase.select('td')[0].text == 'Comp. campo contrario' or \
                        pase.select('td')[0].text == 'Comp. balones largos' or \
                        pase.select('td')[0].text == 'Pases globo certeros' or \
                        pase.select('td')[0].text == 'Pases acertados':
                    pases_tuple['Pases ' + pase.select('td')[0].text] = pase.select('td')[1].text.split(' ')[0]
                    pases_tuple['Pases Porc. ' + pase.select('td')[0].text] = int(pase.select('td')[1].text.
                                                                                  split(' ')[1].strip('(').strip(')').
                                                                                  strip('%')) / 100
                else:
                    pases_tuple['Pases ' + pase.select('td')[0].text] = pase.select('td')[1].text


            defensa_tuple = {}
            for defen in defensa:
                defensa_tuple['Defensa ' + defen.select('td')[0].text] = defen.select('td')[1].text

            otros_tuple = {}
            for otro in otros:
                if otro.select('td')[0].text == 'Comp. regates' or \
                        otro.select('td')[0].text == 'Duelos totales ganados' or \
                        otro.select('td')[0].text == 'Duelos en el suelo ganados' or \
                        otro.select('td')[0].text == 'Duelos aéreos ganados':
                    otros_tuple['Otros ' + otro.select('td')[0].text] = otro.select('td')[1].text.split(' ')[0]
                    otros_tuple['Otros Porc. ' + otro.select('td')[0].text] = int(otro.select('td')[1].text.split(' ')[1].
                                                                           strip('(').strip(')').strip('%')) / 100
                else:
                    otros_tuple['Otros ' + otro.select('td')[0].text] = otro.select('td')[1].text


            tarjetas_tuple = {}
            for tarjeta in tarjetas:
                tarjetas_tuple['Tarjetas ' + tarjeta.select('td')[0].text] = tarjeta.select('td')[1].text

            player_tuple = {'Nombre': playerName, 'Img': player_img}

            player_tuple.update(eventos_tuple)
            player_tuple.update(atacante_tuple)
            player_tuple.update(pases_tuple)
            player_tuple.update(defensa_tuple)
            player_tuple.update(otros_tuple)
            player_tuple.update(tarjetas_tuple)
            if porteria is not None:
                player_tuple.update(porteria_tuple)

            players.append(player_tuple)
            print(len(players))
        except Exception as e:
            print(e)

    df = pd.DataFrame.from_dict(players)

    print(df)

    fileName = '../output/sofascore_' + leagueName + '.csv'
    leagueIndex += 1
    df.to_csv(fileName, index=False)

