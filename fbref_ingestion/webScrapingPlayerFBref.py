
import requests
from bs4 import BeautifulSoup
import pandas as pd
import re
import sys, getopt
import csv
from configuration import Configuration

conf = Configuration()


def scrapeURL(url, league):
    res = requests.get(url)
    #  The next two lines get around the issue with comments breaking the parsing.
    comm = re.compile('<!--|-->')
    soup = BeautifulSoup(comm.sub(',', res.text), 'lxml')
    all_tables = soup.findAll('tbody')
    team_table = all_tables[0]
    player_table = all_tables[1]
    
    #  Parse player_table
    pre_df_player = dict()
    features_wanted_player = {"player", "nationality", "position", "squad", "age", "birth_year", "games",
                              "games_starts", "minutes", "goals", "assists", "pens_made", "pens_att", "cards_yellow",
                              "cards_red", "goals_per90", "assists_per90", "goals_assists_per90", "goals_pens_per90",
                              "goals_assists_pens_per90", "xg", "npxg", "xa", "xg_per90", "xa_per90", "xg_xa_per90",
                              "npxg_per90", "npxg_xa_per90"}
    rows_player = player_table.find_all('tr')
    for row in rows_player:
        if row.find('th', {"scope": "row"}) is not None :
        
            for f in features_wanted_player:
                cell = row.find("td", {"data-stat": f})
                if cell is None:
                    pre_df_player[f] = None
                else:
                    a = cell.text.strip().encode()
                    text = a.decode("utf-8")
                    if f in pre_df_player:
                        pre_df_player[f].append(text)
                    else:
                        pre_df_player[f] = [text]
            pre_df_player['league'] = league
    df_player = pd.DataFrame.from_dict(pre_df_player)
    
    # #Parse team_table
    # pre_df_squad = dict()
    # #Note: features does not contain squad name, it requires special treatment
    # features_wanted_squad = {"players_used", "games", "goals", "assists", "pens_made", "pens_att", "fouls",
    #                          "cards_yellow", "cards_red", "shots_on_target", "goals_perG", "goals_assists_perG",
    #                          "goals_pens_perG", "goals_assists_pens_perG", "shots_on_target_perG", "fouls_perG",
    #                          "cards_perG"}
    # rows_squad = team_table.find_all('tr')
    # for row in rows_squad:
    #     if(row.find('th',{"scope":"row"}) != None):
    #         name = row.find('th',{"data-stat":"squad"}).text.strip().encode().decode("utf-8")
    #         if 'squad' in pre_df_squad:
    #             pre_df_squad['squad'].append(name)
    #         else:
    #             pre_df_squad['squad'] = [name]
    #         for f in features_wanted_squad:
    #             cell = row.find("td",{"data-stat": f})
    #             a = cell.text.strip().encode()
    #             text=a.decode("utf-8")
    #             if f in pre_df_squad:
    #                 pre_df_squad[f].append(text)
    #             else:
    #                 pre_df_squad[f] = [text]
    # df_squad = pd.DataFrame.from_dict(pre_df_squad)
    
    return df_player


df_total = pd.DataFrame()

for url in conf.leagues_fbref:
    k = url.rfind("/")
    output_name = url[k + 1:].replace('-Stats', '')
    league = output_name.replace('-', ' ')
    dfplayer = scrapeURL(url, league)

    dfplayer.to_csv('../output/FBref_' + output_name + '.csv', index=False)
    # df_total = pd.concat([df_total, dfplayer], axis=0)
    # df_squad.to_csv(output_name+"_squad.csv")

# df_total.to_csv("../output/all_leagues_players.csv", index=False)


